from django.shortcuts import render, HttpResponse


# Create your views here.

def home(request):
    return render(request, "StoreApp/home.html")

def store(request):
    return render(request, "StoreApp/store.html")

