from django.shortcuts import render
from .forms import FormularioContacto

# Create your views here.

def contacto(request):
    formularioContacto = FormularioContacto()
    
    if request.method == "POST":
        formularioContacto = FormularioContacto(data=request.POST)
        if formularioContacto.is_valid():
            nombre = request.POST.get("nombre")
            email = request.POST.get("email")
            contenido = request.POST.get("contenido")
            
    return render(request, "contacto/contacto.html", {'miFormulario': formularioContacto})